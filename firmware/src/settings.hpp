#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <Arduino.h>

const uint32_t stepsPerRev   = 4076; // 1:63.68395 ratio or 2037.8864 approximately 2038 steps per rev
const uint32_t secondsPerRev = 300;

const int pinRtcIO   = 4;
const int pinRtcSCLK = 5;
const int pinRtcCE   = 2;

const int pinButton  = 3;

const int pinLED     = 13;

int pinMotor[] = { 6, 8, 7, 9 };

const int normalSpeed = 80; // speed clock normally moves
const int fastSpeed   = 200; // speed when button pressed

const int stepsToWrite = 512;

// wait for reset in 100ms intervals
const int resetWaitCount = 10;

// minimum delta in seconds to start moving
const uint32_t minDeltaSecs = 15; // move every 15 seconds

#endif // SETTINGS_HPP