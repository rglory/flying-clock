#include "settings.hpp"

#include <ThreeWire.h>  
#include <RtcDS1302.h>
#include <AccelStepper.h>

ThreeWire myWire( pinRtcIO,pinRtcSCLK, pinRtcCE ); // IO, SCLK, CE
RtcDS1302<ThreeWire> rtc( myWire );
AccelStepper stepper( AccelStepper::HALF4WIRE, pinMotor[0], pinMotor[1], pinMotor[2], pinMotor[3], false );

int errorCode = 0;

void reportError()
{
    static int count = 0;
    static bool ledEna = false;
    if( ledEna ) {
        if( ++count == errorCode ) {
            count = 0;
            delay( 1000 );
        }
        else delay( 100 );
    } else
        delay( 50 ); 
    digitalWrite( pinLED, ledEna );
    ledEna = !ledEna;
}

const auto invalidTicks  = uint32_t( -1 );
const auto secondsPerDay = uint32_t( 12 ) * 60 * 60;

struct data {
    uint32_t ticks;
    uint32_t crc;

    uint32_t calcCRC() const  { return ticks ^ invalidTicks; }

    data() = default;
    data( uint32_t t ) : ticks( t ), crc( calcCRC() ) {}

    bool valid() const { return crc == calcCRC(); }
};

void writeRtcMemory( uint32_t ticks )
{
    data d( ticks );
    rtc.SetMemory( reinterpret_cast<uint8_t *>( &d ), sizeof( d ) );
}

uint32_t readRtcMemory()
{
    data d;
    rtc.GetMemory( reinterpret_cast<uint8_t *>( &d ), sizeof( d ) );
    return d.valid() ? d.ticks : invalidTicks;
}

uint32_t secondsToSteps( uint32_t sc )
{
    return sc * stepsPerRev / secondsPerRev;
}

uint32_t currentPos = 0;
uint32_t targetPos = 0;

void setup() 
{
    pinMode( pinButton, INPUT_PULLUP );
    pinMode( pinLED, OUTPUT );

    digitalWrite( pinLED, 1 );
    Serial.begin( 19200 );
    rtc.Begin();
    rtc.SetIsWriteProtected( false );

    stepper.setMaxSpeed( fastSpeed );

    int count = 0;
    while( count < resetWaitCount and digitalRead( pinButton ) == 0 ) {
        ++count;
        delay( 100 );
    }

    if( count == resetWaitCount ) {
        Serial.println( "Button press detected, reset" );
        RtcDateTime dt( secondsPerDay );
        rtc.SetDateTime( dt );
        rtc.SetIsRunning( true );
        auto curr = secondsToSteps( dt.TotalSeconds() % secondsPerDay );
        writeRtcMemory( curr );
        Serial.print( "initialized with:" );
        Serial.println( curr ); 
        digitalWrite( pinLED, 0 );
        while( digitalRead( pinButton ) == 0 ) // wait until button is depressed
            delay( 50 );
    } else {
        Serial.println( "Normal start" );
        digitalWrite( pinLED, 0 );
    }
    if( not rtc.IsDateTimeValid() ) {
        Serial.println( "invalid date time, check connection" );
        errorCode = 1;
        return;
    }
    currentPos = readRtcMemory();
    Serial.print( "started with pos:" );
    Serial.println( currentPos );
    if( currentPos == invalidTicks ) {
        errorCode = 2;
        return;
    }
}

enum Mode { mWait, mNormalMove, mFastMove };

bool buttonPressed()
{
   return digitalRead( pinButton ) == 0;
}

void stepperStart( bool fast )
{
    stepper.enableOutputs();
    stepper.setSpeed( fast ? fastSpeed : normalSpeed );
}

void stepperStop()
{
    stepper.setCurrentPosition( 0 );
    stepper.disableOutputs();
}

void loop() 
{
    if( errorCode ) 
        return reportError();

    static Mode mode = mWait;

    const auto stepsPerDay = secondsToSteps( secondsPerDay );

    if( mode == mNormalMove ) {
        if( not stepper.runSpeed() ) {
            yield();
            return;
        }
        if( ++currentPos == stepsPerDay )
            currentPos = 0;

        static int stepsSinceWrite = 0;
        if( ++stepsSinceWrite == stepsToWrite or currentPos == targetPos ) {
            writeRtcMemory( currentPos );
            stepsSinceWrite = 0;
        }

        if( currentPos == targetPos ) {
            mode = mWait;
            stepperStop();
        }

        return;
    }


    if( mode == mFastMove ) {
        if( not stepper.runSpeed() ) {
            yield();
            return;
        }
        if( not buttonPressed() ) {
            mode = mWait;
            stepperStop();
        }
        return;
    }

    // mode is wait
    if( buttonPressed() ) {
        Serial.println( "moving fast..." );
        mode = mFastMove;
        stepperStart( true );
        return;
    }
    
    auto dt = rtc.GetDateTime();
    targetPos  = secondsToSteps( dt.TotalSeconds() % secondsPerDay );
    auto d = targetPos + ( currentPos > targetPos ? stepsPerDay : 0 ) - currentPos;
    if( d >= secondsToSteps( minDeltaSecs ) ) {
        Serial.print( "normal moving from:" );
        Serial.print( currentPos );
        Serial.print( " target:" );
        Serial.println( targetPos );

        mode = mNormalMove;
        stepperStart( false );
        return;
    }
    delay(200);
}

